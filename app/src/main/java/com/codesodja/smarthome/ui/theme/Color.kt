package com.codesodja.smarthome.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

//SMART HOME COLOR
val HomeBackground =  Color(0xffb5977d)
val DetailBackground =  Color(0xffa8886c)
val MenuBackground =  Color(0xffbf2f2f2)
val BtnAndItemSelectedColor =  Color(0xffa8866b)
val WhiteColor =  Color(0xffffffff)
val SelectedIconColor =  Color(0xff67615c)
val TurnOnColor =  Color(0xffffb86c)
val TurnOffColor =  Color(0xffc7c7c7)
val MenuTitleColor =  Color(0xff837f7a)
val FeatureTitleColor =  Color(0xffaeada9)
val FeatureSubTitleColor =  Color(0xffd7d7d5)
val LightTextColor =  Color(0xffddc9b7)
val DetailsHeaderIconColor =  Color(0xff68625b)
val DetailsHeaderNumberColor =  Color(0xff6e6d69)
val DetailsHeaderTextColor =  Color(0xffbbbabb)
val DetailsItemClickedTextColor =  Color(0xffffbc75)